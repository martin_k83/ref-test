#!/usr/bin/env bash
for TAGNO in {1..100}; do echo $TAGNO > .gitkeep && git add . && git commit -m "Commit $TAGNO" && git tag 0."$TAGNO" && git push && git push --tags; done
